from pymongo import MongoClient
from chunker import Chunker
import os
import boto3
from botocore.exceptions import ClientError
from kafka import KafkaProducer
import logging
import json

# AWS S3 config
s3 = boto3.resource('s3')
BUCKET_NAME = os.environ.get('BUCKET_NAME')
s3_bucket = s3.Bucket(BUCKET_NAME)

# MongoDB config
client = MongoClient(os.environ.get('MONGODB_CLIENT'))
db=client["metadata"]

# logging.basicConfig(level=logging.DEBUG)
# logger = logging.getLogger(__name__)

# Kafka config
producer = KafkaProducer(
    bootstrap_servers=[os.environ.get('KAFKA_BROKER_URL')], 
    value_serializer=lambda value: json.dumps(value).encode())
UPLOAD_TOPIC = os.environ.get('UPLOAD_TOPIC')

USERNAME = os.environ.get('USERNAME')


class Indexer:

    def __init__(self):
        
        self.chunker = Chunker()


    def index(self, src_path):

        print("indexing")

        chunks = self.chunker.split(src_path)

        print("chunks")
        for x in chunks.chunks:
            print(chunks)
        print(len(chunks.chunks))

            

        print("split")

        path = os.path.split(src_path)
        st_ino = os.stat(src_path)[1] # using physical location as file id

        found = db["local_metadata"].find({"$and":[ {"doc_id": st_ino}, {"object.user.username": USERNAME}]}, {"checksum.adler32checksum":1, "checksum.md5checksum":1 ,"_id":0})

        # test
        print("found")
        print(found.count())


        to_upload = set(chunks.get_chunks()) # contents to upload to s3
        to_remove = [] # remove from s3 & local metadata db

        for record in found:

            print(record)
            print("----")

            chunk_index = chunks.get_order_checksum(record["checksum"]["adler32checksum"], record["checksum"]["md5checksum"])

            if chunk_index is not None:

                to_upload.remove(chunks.get_chunk(chunk_index))

            else:

                to_remove.append(record["checksum"])


        print("Contents to remove from local metadata db and s3")
        print(to_remove)

        print("Contents to upload to local metadata db and s3")
        print(to_upload)


        if len(to_remove) > 0:

            print("Removing from s3 and local metadata db")

            # remove from local db
            response = db["local_metadata"].remove({'checksum':{'$in':to_remove}})

            print("response")
            print(response)

            to_delete_s3 = {'Objects': [{'Key': '_'.join([str(checksum['adler32checksum']), str(checksum['md5checksum'])]) + '.txt'} for checksum in to_remove]}

            print("to_delete_s3")
            print(to_delete_s3)

            # remove from s3
            # TODO: Since cross users share chunks for same files, need to check if chunk is used by more than one user before deleting from s3,
            # Do this by checking with extenral db for more than one user.
            response = s3_bucket.delete_objects(Bucket=BUCKET_NAME, Delete=to_delete_s3)

            print("response")
            print(response)


        db_upload = []
        for adler32, md5_chunk in chunks.get_checksums().items():

            for md5, chunk_index in md5_chunk.items():

                chunk = chunks.get_chunk(chunk_index)

                document1 = {
                    'doc_id': st_ino,
                    'checksum': {
                        'adler32checksum': adler32,
                        'md5checksum': md5
                    }
                }

                document2 = {
                    'chunk_order': chunks.get_order(chunk),
                    'object': {
                        'filename': path[1],
                        # 'filesize': ,
                        'filepath': path[0],
                        'user': {
                            'username': USERNAME
                        }
                    }
                }

                if chunk in to_upload: # user has not uploaded to s3 and dne in local db, therefore upload to s3 & local db

                    document1.update(document2)

                    print(document1)
                    
                    db_upload.append(document1)

                    bucket_key = '_'.join([str(adler32), str(md5)])

                    # check if another user has uploaded same file to s3
                    # TODO: replace this call with a check to the external metadata db
                    if not check_key_exists_s3(s3, BUCKET_NAME, bucket_key):

                        print("Uploading to s3")

                        response = s3_bucket.put_object(Body=chunk, Key=bucket_key + '.txt')

                        print("response:")
                        print(response)


                else: # already exists in s3 and local db, need to update chunk order, file path

                    db["local_metadata"].update_one(document1, {'$set': document2})


        # update local metadata db

        print("db_upload")
        print(db_upload)

        if len(db_upload) > 0:

            print("Uploading to local metadata db")

            response = db["local_metadata"].insert_many(db_upload)

            print("response:")
            print(response)



        # synchronize & update external metadata db by sending to kafka
        self.synchronize(db_upload, to_remove, UPLOAD_TOPIC)



        print(to_remove)
        print(db_upload)


        print("done")

        print("----------------------------------------------------------------")


    def synchronize(self, to_upload, to_remove, kafka_topic):

        print("Synchronizing with external metadata db")

        payload = {
            'to_remove': to_remove,
            'to_upload': to_upload
        }

        print(payload)

        #TODO: change encoding format to Avro or another
        producer.send(UPLOAD_TOPIC, payload)


# [{'adler32checksum': 1830740172, 'md5checksum': 'c076d2c75619dc11580d53cd7601b437'}]

# [{'doc_id': 12384898975338935, 'checksum': {'adler32checksum': 536305763, 'md5checksum': '025a90efd92d6907001576b31f670523'}, 
# 'chunk_order': 0, 'object': {'filename': 'test_produce.py', 'filepath': 'C:\\Users\\henry\\Desktop\\Projects\\Python-Workspace\\Dropbox\\src', 
# 'user': {'username': 'henry'}}, '_id': ObjectId('5e2b29a8c3838d3ffc9bf9d1')}]




def check_key_exists_s3(s3, bucket, key):
    """
    Check if key exists in s3 bucket
    """
    try:
        s3.Object(bucket, key).load()
    except ClientError as e:
        return int(e.response['Error']['Code']) != 404
    return True

