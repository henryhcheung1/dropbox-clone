import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from indexer import Indexer
# import argparse
import sys
from os import path

class Watcher:

    def __init__(self, watch_path):
        self.observer = Observer()
        self.DIRECTORY_TO_WATCH = watch_path

        self.event_handler = FileSystemEventHandler()
        self.event_handler.on_any_event = self.on_any_event

        self.indexer = Indexer()


    def on_any_event(self, event):

        # if event.is_directory:
        #     return None

        relative_path = path.relpath(event.src_path, self.DIRECTORY_TO_WATCH)

        if event.event_type == 'created':
            print("Received created event - %s." % event.src_path)

            self.indexer.index(relative_path)

        elif event.event_type == 'modified':
            print("Received modified event - %s." % event.src_path)

            self.indexer.index(relative_path)

        elif event.event_type == 'deleted':
            print("Received deleted event - %s." % event.src_path)



    def run(self):
        self.observer.schedule(self.event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)

                # check for kafka messages

                # would create a chain of cloud uploads??


        except:
            self.observer.stop()
            print("Error")

        self.observer.join()



if __name__ == '__main__':

    #TODO: change to argparse - consider use cases of changing sync folder
    w = Watcher(sys.argv[1])
    w.run()

    print("Terminate")




# from kafka import KafkaConsumer
# from time import sleep

# consumerA1 = KafkaConsumer('A',
#                          auto_offset_reset='earliest',
#                          enable_auto_commit=True,
#                          auto_commit_interval_ms=1000,
#                          group_id='A',
#                          bootstrap_servers=['localhost:9092'])



# print("A1")
# # sleep(5)
# print("A!")

# # M = 10
# # i = 0
# for message in consumerA1:
#     print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
#                                           message.offset, message.key,
#                                           message.value))
