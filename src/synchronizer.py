from kafka import KafkaConsumer, KafkaProducer
import os
from pymongo import MongoClient


# Kafka config
UPLOAD_TOPIC = os.environ.get('UPLOAD_TOPIC')
SYNCHRONIZATION_GROUP = os.environ.get('SYNCHRONIZATION_GROUP')

# Mongodb config
client = MongoClient(os.environ.get('MONGODB_SYNC'))
db=client["metadata"]


class Synchronizer():

    def __init__(self):
        pass

    def run(self):

        consumer = KafkaConsumer(UPLOAD_TOPIC,
                                auto_offset_reset='earliest',
                                enable_auto_commit=True,
                                auto_commit_interval_ms=1000,
                                group_id=SYNCHRONIZATION_GROUP,
                                bootstrap_servers=[os.environ.get('KAFKA_BROKER_URL')])

        
        for message in consumer:
            # client message to upload
            print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                                message.offset, message.key,
                                                message.value))

            



if __name__ == '__main__':

    synchronization_service = Synchronizer()
    synchronization_service.run()
    
    print("Terminate")