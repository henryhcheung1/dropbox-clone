import hashlib
import zlib

# BLOCK_SIZE = 4096

class Chunker:

    def __init__(self, chunk_size=10000):
        self.CHUNK_SIZE = chunk_size # size of each file in B


    def split(self, filename):
        """
        Splits file into chunks of size CHUNK_SIZE bytes
        """


        print("chunking")


        # i = 0
        chunks = Chunks()
        # chunks = []
        with open(filename, 'rb') as infile:

            while True:
                # read chunksize bytes
                chunk = infile.read(self.CHUNK_SIZE)
                if not chunk: 
                    break

                # hold chunks in memory for now
                chunks.append(chunk)


                # # test
                # outfilename = str(i) + filename
                # with open(outfilename, 'wb') as outfile:
                #     outfile.write(chunk)

                # i += 1

        return chunks


    def combine(self):

        files = ['0img_snow.jpg', '1img_snow.jpg', '2img_snow.jpg']
        
        out_data = b''
        for fn in files:
            with open(fn, 'rb') as fp:
                out_data += fp.read()

        with open('new_image.jpg', 'wb') as fp:
            fp.write(out_data)



class Chunks:
    """
    Class to hold file chunks and checksums
    """

    def __init__(self):

        self.chunks = []
        self.checksums = {}
        # self.chunk = chunk
        # self.adler_checksum = adler32_chunk(chunk)
        # self.md5_checksum = md5_chunk(chunk)



    def append(self, chunk):

        adler_checksum = adler32_chunk(chunk)

        self.chunks.append(chunk)
        self.checksums.setdefault(adler_checksum, {})
        self.checksums[adler_checksum][md5_chunk(chunk)] = len(self.chunks) - 1


    def get_order(self, chunk):
        adler32 = self.checksums.get(adler32_chunk(chunk))

        if adler32:
            return adler32.get(md5_chunk(chunk))

        return None


    def get_order_checksum(self, alder32_checksum, md5_checksum):

        adler32 = self.checksums.get(alder32_checksum)

        if adler32:
            return adler32.get(md5_checksum)

        return None


    def get_chunk(self, i):

        return self.chunks[i]


    def get_chunks(self):

        return self.chunks


    def get_checksums(self):

        return self.checksums


# checksum helper functions
def md5_chunk(chunk):
    """
    Returns md5 checksum for chunk
    """
    m = hashlib.md5()
    m.update(chunk)
    return m.hexdigest()


def adler32_chunk(chunk):
    """
    Returns adler32 checksum for chunk
    """
    return zlib.adler32(chunk)




    # chunking algortihm to deermine which chunks have been modified by the user

# chunker = Chunker()

# chunker.split("img_snow.jpg")
# chunker.combine()

